---
title: Algorave Brasil 2020 (pt)
pubDate: 2020-12-12
description: Algorave 2020
---

<div class="description">
  <ul class="horizontal unstyled padded" style={{ maxWidth: "22rem", marginLeft: 0 }}>
    <li><a href="/eventos/2020/pt">português</a></li>
    <li><a href="/eventos/2020/es">español</a></li>
    <li><a href="/eventos/2020/en">english</a></li>
  </ul>
  <p>Este é o site da edição anual da Algorave Brasil, um evento Brasileiro organizado pela comunidade de mesmo nome, Algorave Brasil, para performances ao vivo de live coding nos dias 12 e 13 de Dezembro de 2020, das 14h as 18h, incluindo artistas visuais, artistas sonoros, músicos, não-músicos, programadoras, mestres em gambiologia, engenheiras, curiosos, iniciantes ou experientes.</p>
  <p>O principal objetivo do evento é aproximar os interessados em <a href="https://en.wikipedia.org/wiki/Live_coding">live coding</a> no Brasil.</p>

<iframe
  width="560"
  height="315"
  src="https://www.youtube.com/embed/jPU2nTxoyJk?start=1710"
  frameborder="0"
  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
  allowfullscreen
></iframe>
&nbsp;
<iframe
  width="560"
  height="315"
  src="https://www.youtube.com/embed/tMRAJQ0LWAA?start=1444"
  frameborder="0"
  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
  allowfullscreen
></iframe>

<h1>Participe!</h1>

<p>
  <em>inscrições encerradas</em>
  <s>
    Para participar basta incluir o seu nome e os detalhes da sua performance na
    planilha abaixo.
  </s>
</p>

  <ul>
    <li><a href="https://docs.google.com/spreadsheets/d/1TsNKt7erfQlH0SqALYQaoy9c1n_xuj30nMjEM-sqmzI/edit#gid=0">https://docs.google.com/spreadsheets/d/1TsNKt7erfQlH0SqALYQaoy9c1n_xuj30nMjEM-sqmzI/edit#gid=0</a></li>
  </ul>

  <h2>Instruções de transmissão (streaming)</h2>

  <ul>
    <li><a href="/eventos/2020-transmissao/pt">Veja aqui as instruções de como transmitir a sua performance</a>.</li>
  </ul>

  <h1>Acompanhe ao vivo!</h1>
  <p>
    <ul>
      <li><a href="https://www.youtube.com/channel/UChqe--qhcErjPjCA9oJgq0w">YouTube</a></li>
      <li><a href="https://www.twitch.tv/algoravebr">Twitch</a></li>
      <li>rtmp://biniou.net/algorave/algoravebrasil</li>
    </ul>
  </p>
  <h1>Programação</h1>
  <iframe width="100%" height="650px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vT05KeqHcZ-VRtK6nmakNdyJa3lJpCbljzta8Q_K5VwB-YvMKinWri_K62F9mTngvX3RAYvIfmxhW-m/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>
  <h1>Dúvidas?</h1>
  <p>
    Fale conosco através do nosso <a href={'tg://join?invite=BCotLVCAJTQ3fW5E-j9DVA'}>grupo no telegram</a>.
    Se você quer participar mas não sabe como, se precisa de uma mãozinha ou tem qualquer outra dúvida sobre o evento, fique à vontade para nos procurar pelo telegram, ele tem sido o meio "oficial" de comunicação com a comunidade Algorave Brasil.
  </p>
  <h1>Agradecimentos</h1>
  <p>A toda comunidade Algorave Brasil,
  em especial aos artistas participantes:
  djalgoritmo,
  irisS,
  n0p | flavio,
  Indizível,
  Pietro Bapthysthe,
  garrafada,
  Lowbin,
  Alexandre Rangel,
  ghales,
  Gil Fuser,
  Bedran & Kastrup,
  igor & vito,
  berin | vinícius,
  beise,
  euFraktus X,
  Hernani Villaseñor,
  BSBLOrk,
  fmiramar,
  diegodukao
  e aos organizadores:
  Igor,
  ghales,
  Gil Fuser,
  berin,
  vinícius,
  beise e
  diegodukao. (Por favor nos avise se esquecemos de adicionar seu nome aqui).
</p>

<p>
  Um agradecimento especial também ao Olivier "oliv3" Girondel, desenvolvedor do
  software livre <a href="https://biniou.net">Le Biniou</a>, por oferecer o
  servidor RTMP utilizado para transmissão deste evento.
</p>

</div>
