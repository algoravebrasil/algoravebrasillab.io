---
title: Algorave Brasil 2021 (en)
pubDate: 2021-12-11
description: Algorave 2021
---

<div className="description">
<ul className="horizontal unstyled padded" style={{ maxWidth: "22rem", marginLeft: 0 }}>
  <li><a href="/eventos/2021/pt">português</a></li>
  <li>español</li>
  <li><a href="/eventos/2021/en">english</a></li>
</ul>
<p> This is the page for the annual edition of Algorave Brasil, a Brazilian event organized by our community for online live-coding performances on December the 11th and 12th, 2021, from 2PM to 6PM (Sao Paulo local time), the event includes visual and sound artists, musicians, non-musicians, programmers, work-around masters, engineers, curious people, beginners or experienced programmers alike.</p>
<p>The purpose of the event is to gather those interested in <a href="https://en.wikipedia.org/wiki/Live_coding">live coding</a> in Brazil.</p>

<p><a href="https://www.youtube.com/watch?v=uP_a_kXsCto" target="_blank">Streamming 11th (Saturday)</a></p>
<p><a href="https://www.youtube.com/watch?v=8TrtuSZpx10" target="_blank">Streamming 12th (Sunday)</a></p>

<h1>Join us!</h1>
<p>(<em>closed for now</em>) <s>To participate in the event, just include your name and perfomance details on the spreadsheet below</s></p>

<h2>Streaming Instructions</h2>
<ul>
  <li><a href="/eventos/2021-transmissao/en">Check out our instructions on how to stream your performance</a>.</li>
</ul>
<h1>Watch it Live!</h1>
<p>
  <ul>
    <li><a href="https://www.youtube.com/channel/UChqe--qhcErjPjCA9oJgq0w">YouTube</a></li>
  </ul>
</p>
<h1>Event Schedule</h1>
<iframe width="100%" height="650px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vT05KeqHcZ-VRtK6nmakNdyJa3lJpCbljzta8Q_K5VwB-YvMKinWri_K62F9mTngvX3RAYvIfmxhW-m/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>
<h1>Questions?</h1>
<p>Reach us through our <a href='tg://join?invite=BCotLVCAJTQ3fW5E-j9DVA'>Telegram group</a>. If you want to participate but don't know how, if you need a hand or has any other questions about the event, feel free to reach us through telegram - it's been our "very official" means of communication for now.</p>
<h1>Thanks</h1>
<p>To the entire Algorave Brasil community, in particular to the participating artists:
Pietro Bapthysthe,
fmiramar,
Bruno Gola,
diegodorado,
alovictor,
smosgasbord,
lowbin,
Rafrobeat,
euFraktus_X,
Santiago Ramírez Camarena,
diegodukao,
beise,
Indizível (brunorohde),
Gil Fuser,
Mega Jam FoxDot,
Ángel Jara,
igor | vito,
BSBLOrk.</p>
</div>
