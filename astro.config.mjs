import { defineConfig } from 'astro/config';
import mdx from '@astrojs/mdx';
import sitemap from '@astrojs/sitemap';
import image from '@astrojs/image';

// https://astro.build/config
export default defineConfig({
  site: 'https://algoravebrasil.gitlab.io/',
  output: 'static',
  integrations: [
    mdx(),
    sitemap(),
    image({
      serviceEntryPoint: '@astrojs/image/sharp',
    }),
  ],
  // Para habilitar hot-reloading, ou seja 
  // Atualização automática do navegador quando um arquivo é alterado
  vite: {
    server: {
      host: "0.0.0.0",
      hmr: { clientPort: 3000 },
      port: 3000,
      watch: { usePolling: true }
    }
  },
  // redirects to keep old links alive
  redirects: {
    // /2020/* redirects
    '/admin/config.yml': '/config.yml',
    '/2020/en': '/eventos/2020/en',
    '/2020/es': '/eventos/2020/es',
    '/2020/pt': '/eventos/2020/pt',
    '/2020-transmissao/en': '/eventos/2020-transmissao/en',
    '/2020-transmissao/es': '/eventos/2020-transmissao/es',
    '/2020-transmissao/pt': '/eventos/2020-transmissao/pt',
    // /2021/* redirects
    '/2021/en': '/eventos/2021/en',
    '/2021/es': '/eventos/2021/es',
    '/2021/pt': '/eventos/2021/pt',
    '/2021-transmissao/en': '/eventos/2021-transmissao/en',
    '/2021-transmissao/es': '/eventos/2021-transmissao/es',
    '/2021-transmissao/pt': '/eventos/2021-transmissao/pt',
    // /2022/* redirects
    '/2022/en': '/eventos/2022/en',
    '/2022/es': '/eventos/2022/es',
    '/2022/pt': '/eventos/2022/pt',
    '/2022-transmissao/en': '/eventos/2022-transmissao/en',
    '/2022-transmissao/es': '/eventos/2022-transmissao/es',
    '/2022-transmissao/pt': '/eventos/2022-transmissao/pt'
  }
});
