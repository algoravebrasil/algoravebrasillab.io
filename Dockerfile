FROM node:20-alpine

COPY . /home/node/app
WORKDIR /home/node/app
RUN npm i

ENV NODE_ENV development
# USER node

# EXPOSE 3000

CMD ["npm", "start"]